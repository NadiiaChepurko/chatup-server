package com.org.chatup.Util;

import com.google.common.collect.ImmutableList;

import java.util.List;

/**
 * Created by nadiia.
 */
public class Utils {

    public static <T> List<T> toList(T... objects) {
        ImmutableList.Builder<T> builder = ImmutableList.builder();
        for (T pair : objects) {
            builder.add(pair);
        }
        return builder.build();
    }

}

package com.org.chatup.Util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.Charset;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;
import javax.sql.DataSource;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONException;
import org.json.JSONObject;

import com.org.chatup.User;

public final class Helper {
	
	public static JSONObject getEncodedJson(HttpServletRequest request) {
		StringBuffer jsonBuilder = new StringBuffer();
		String line = null;
		
		try {
		  BufferedReader reader = request.getReader();
		  while ((line = reader.readLine()) != null)
			  jsonBuilder.append(line);
		  
		  return new JSONObject(jsonBuilder.toString());
		  
		} catch (Exception e) {
			e.printStackTrace();
			return null; 
		}
	}
	
	
	/**
	 * Get all registered devices with this regId
	 */
	public static List<User> getDeviceInfo(String regId, DataSource dataSource) throws SQLException {
		final String SELECT_QUERY = "SELECT * FROM " + "USERS" +" WHERE gcm_regId='" + regId + "'";
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<User> userList = new ArrayList<User>();
		Connection connection = dataSource.getConnection();
		
		try {
			preparedStatement = connection.prepareStatement(SELECT_QUERY);
			resultSet = preparedStatement.executeQuery();
			
			while(resultSet.next()) {
				User user = new User();
				user.setName(resultSet.getString("name"));
				user.setEmail(resultSet.getString("email"));
				user.setIsActive(resultSet.getBoolean("is_active"));
				user.setLastActive(resultSet.getString("last_active"));
				user.setGcmRegId(resultSet.getString("gcm_regId"));
				
				userList.add(user);
			}

			connection.close();
			
		} catch(SQLException e) {
			e.printStackTrace();
			throw e;
			
		} finally {
			try { if(connection != null) { connection.close(); } } catch (SQLException e) { throw e;}
			try { if(preparedStatement != null) { preparedStatement.close(); } } catch (SQLException e) { throw e;}
			try { if(resultSet != null) { resultSet.close(); } } catch (SQLException e) { throw e;}
		}
		
		return userList;
	}
	
	
	/**
	 * Remove all registered devices with this regId
	 */
	public static void removeDeviceInfo(String regId, DataSource dataSource) throws SQLException {
		final String UPDATE_QUERY = "UPDATE " + "USERS" +" SET is_active = ? WHERE gcm_regId='" + regId + "'";
		PreparedStatement preparedStatement = null;
		Connection connection = dataSource.getConnection();
		
		try {
			preparedStatement = connection.prepareStatement(UPDATE_QUERY);
			preparedStatement.setInt(1, 0);
			preparedStatement.executeQuery();
			
			connection.close();
			
		} catch(SQLException e) {
			e.printStackTrace();
			throw e;
			
		} finally {
			try { if(preparedStatement != null) { preparedStatement.close(); } } catch (SQLException e) { throw e;}
			try { if(connection != null) { connection.close(); } } catch (SQLException e) { throw e;}
		}
	}
	
	
	/**
	 * Inserts a new device into Database.
	 */
	public static void insertDeviceInfo(String name, String email, String regId, DataSource dataSource) throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		final String INSERT_QUERY = "INSERT INTO " + "USERS" +" (name, email, is_active, last_active, gcm_regId) VALUES (?, ?, ?, ?, ?)";
		PreparedStatement preparedStatement = null;
		Connection connection = dataSource.getConnection();
		
		try {
			preparedStatement = connection.prepareStatement(INSERT_QUERY);
			
			preparedStatement.setString(1, name);
			String currentTime = getCurrentTime();
			preparedStatement.setString(2, email);
			preparedStatement.setBoolean(3, true);
			preparedStatement.setString(4, currentTime);
			preparedStatement.setString(5, regId);
			
			preparedStatement.addBatch();
			preparedStatement.executeBatch();
			connection.close();
			
		} catch(SQLException e) {
			e.printStackTrace();
			throw e;
			
		} finally {
			try { if(preparedStatement != null) { preparedStatement.close(); } } catch (SQLException e) { throw e;}
			try { if(connection != null) { connection.close(); } } catch (SQLException e) { throw e;}
		}
	}

	/**
	 * Checks if the device is registered.
	 */
	public static boolean containsDeviceInfo(String regId, DataSource dataSource) throws SQLException {
		List<User> userList = new ArrayList<User>();
		
		try {
			userList = getDeviceInfo(regId, dataSource);
			if(userList.size() < 1) {
				return false;
			}
		} catch(SQLException e) {
			e.printStackTrace();
			throw e;	
		}
		
		return true;
	}
	
	
	/**
	 * Checks if the device is registered.
	 */
	public static void updateDeviceLastActiveTime(String regId, DataSource dataSource) throws SQLException {
		PreparedStatement preparedStatement = null;
		Connection conn = dataSource.getConnection();
		final String UPDATE_QUERY = "UPDATE " + "USERS" +" SET last_active = ? WHERE gcm_regId='" + regId + "'";
		
		try {
			preparedStatement = conn.prepareStatement(UPDATE_QUERY);
			preparedStatement.setString(1, getCurrentTime());
			preparedStatement.addBatch();
			preparedStatement.executeBatch();
			conn.close();
			
		} catch(SQLException e) {
			e.printStackTrace();
			throw e;
			
		} finally {
			try { if(conn != null) { conn.close(); } } catch (SQLException e) { throw e;}
			try { if(preparedStatement != null) { preparedStatement.close(); } } catch (SQLException e) { throw e;}
		}
	}
	
	
	public static String getCurrentTime() {
		Date dt = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		return sdf.format(dt);
	}
	
	
	public static String readAll(Reader rd) throws IOException {
	    StringBuilder sb = new StringBuilder();
	    int cp;
	    while ((cp = rd.read()) != -1) {
	      sb.append((char) cp);
	    }
	    return sb.toString();
	  }
	
	
	
	public static String readStringFromUrlAuth(String url, Map<String, String> headers) throws ClientProtocolException, IOException {
		HttpClient httpClient = HttpClientBuilder.create().build();
		HttpGet httpRequest = new HttpGet(url);
		
		if(headers != null) {
			Iterator it = headers.entrySet().iterator();
			while(it.hasNext()) {
				Map.Entry<String, String> pair = (Entry<String, String>) it.next();
				httpRequest.setHeader(pair.getKey(), pair.getValue());
			}
		}
		
		HttpResponse httpResponse = httpClient.execute(httpRequest);
		
		BufferedReader rd = new BufferedReader(new InputStreamReader(httpResponse.getEntity().getContent(), Charset.forName("UTF-8")));
		String text = readAll(rd);
		
		return text;
	}

	
	
	public static JSONObject readJsonFromUrl(String url, Map<String, String> headers) throws IOException, JSONException {
		String jsonText = readStringFromUrlAuth(url, headers);
		JSONObject json = new JSONObject(jsonText);
		
		return json;
	}
}

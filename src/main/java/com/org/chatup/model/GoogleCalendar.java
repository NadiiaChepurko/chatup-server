package com.org.chatup.model;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

import org.joda.time.DateTime;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.org.chatup.Util.Helper;

public class GoogleCalendar {
	private String CALENDAR_ACCESS_TOKEN;
	private String CONTACTS_ACCESS_TOKEN;
	private final String queryType = "&q=";
	private List<String> emailList;
	private final String CALENDAR_ENDPOINT = "https://www.googleapis.com/calendar/v3/";
	private final String CONTACTS_ENDPOINT = "https://www.google.com/m8/feeds/";
	private static final int MAX_RESULTS = 5; 
	private String OPTIONS = "?maxResults=" + MAX_RESULTS + "&showHiddenInvitations=true&alwaysIncludeEmail=true";
	
	public GoogleCalendar(String query, String contacts_access_token, String calendar_access_token) {
		this.CALENDAR_ACCESS_TOKEN = calendar_access_token;
		this.CONTACTS_ACCESS_TOKEN = contacts_access_token;
		try {
			analyzeQuery(query.trim());
		} catch (IOException | JSONException e) {
			e.printStackTrace();
		}
	}
	
	public String getEvents() throws IOException, JSONException {

		Map<String, String> headers = new HashMap<String, String>();
		headers.put("Authorization", "Bearer " + this.CALENDAR_ACCESS_TOKEN);
		String result = "";
		
		if(emailList == null) {
			return "emailList is NULL";
		}
		Set<String> eventIds = new HashSet<String>();
		for(String email : emailList) {
			String url = this.CALENDAR_ENDPOINT + "calendars/primary/events" + OPTIONS + this.queryType + email;
			String response = null;
			JSONArray calendarItemsList;
			System.out.println(url);
			
			JSONObject json = Helper.readJsonFromUrl(url.replace(" ", "%20"), headers);
//			System.out.println(json);
		    if(json.has("items")) {
		    	calendarItemsList = json.getJSONArray("items");
		    	if(calendarItemsList.length() > 0) {
			    	response = getEventsDetails(calendarItemsList, eventIds);
			    }
		    	
		    }
		    
		    if(response == null || response.trim().length() < 1) {
		    	continue;
		    }
		    
		    if(response.trim().length() > 1) {
		    	result += response.trim();
		    }
		}
	    
		if(result.trim().length() < 1) {
			result = "Couldn't find any events";
		}
		
		return "GOOGLE CALENDAR\n" + result;
	}
	
	private String getEventsDetails(JSONArray calendarItemsList, Set<String> eventIds) throws JSONException, MalformedURLException, IOException {
		String title, status, duration;
		StringBuilder result = new StringBuilder();
		
		for(int i = 0; i < calendarItemsList.length(); i++) {
			JSONObject event = calendarItemsList.getJSONObject(i);
			
			if( "cancelled".equals(event.getString("status")) || "Some title".equals(event.getString("summary")) || eventIds.contains(event.getString("id")) ) {
				continue;
			}
			
			status = event.getString("status");
			title = event.getString("summary");
			eventIds.add(event.getString("id"));
			
			if(! event.has("start") || ! event.has("end")) {
				continue;
			}
			JSONObject startDate = event.getJSONObject("start");
			if(startDate.has("date")) {
				// All-day event
				duration = startDate.getString("date");
			} else {
				duration = startDate.getString("dateTime") + " - " + event.getJSONObject("end").getString("dateTime");
			}
/*			
			if(event.getJSONObject("exportLinks").has("text/plain")) {
				fileContents = readStringFromUrlAuth(event.getJSONObject("exportLinks").getString("text/plain"), ACCESS_TOKEN);
				
				searchResult = searchQuery(fileContents);
				if(searchResult.trim().length() > 0) {
					result.append("File: " + title + "\n");
					result.append(searchQuery(fileContents) + "\n\n");
				}
			}
*/		
			result.append(title + "\n" + status + "\n" + duration + "\n\n");
		}
		
		return result.toString();
	}
	
	private void analyzeQuery(String query) throws IOException, JSONException {
		emailList = new ArrayList<String>();
		query = findKeywords(query);
		
		if(query.length() > 1) {
			String CONTACTS_OPTIONS = "?alt=json";
			JSONArray contactsList;
			String url;
			
			for(String queryWord : query.split(" ")) {
				url = CONTACTS_ENDPOINT + "contacts/default/full" + CONTACTS_OPTIONS + this.queryType + queryWord;
				
				Map<String, String> headers = new HashMap<String, String>();
				headers.put("Authorization", "Bearer " + this.CONTACTS_ACCESS_TOKEN);
				headers.put("GData-Version", "3.0");
				
				JSONObject json = Helper.readJsonFromUrl(url.replace(" ", "%20"), headers);
//				System.out.println(url);
				if(json.has("feed")) {
					
					if(json.getJSONObject("feed").getJSONObject("openSearch$totalResults").getInt("$t") < 1) {
						continue;
					}
					contactsList = json.getJSONObject("feed").getJSONArray("entry");
					
					for(int i = 0; i < contactsList.length(); i++) {
						JSONObject contact = contactsList.getJSONObject(i);
						
						if(! contact.has("gd$email")) {
							continue;
						}
						
						JSONArray jsonEmail = contact.getJSONArray("gd$email");
						for(int j = 0; j < jsonEmail.length(); j++) {
//							System.out.println(jsonEmail.getJSONObject(j).getString("address"));
							emailList.add(jsonEmail.getJSONObject(j).getString("address"));
						}
					}
					
				} else {
					continue;
				}
			}
		}
		
		emailList.add(query);
	}
	
	private String findKeywords(String query) {
		String timeMin = "", timeMax = "";
		for(String word : query.split(" ")) {
			if( Pattern.compile(Pattern.quote("today"), Pattern.CASE_INSENSITIVE).matcher(word).find() ) {
				timeMin = getTime().toString();
				timeMax = getTime().plusDays(1).toString();
				query = query.replace(word, "").trim();
				break;
				
			} else if( Pattern.compile(Pattern.quote("previous"), Pattern.CASE_INSENSITIVE).matcher(word).find() ||
					Pattern.compile(Pattern.quote("last"), Pattern.CASE_INSENSITIVE).matcher(word).find() ) {
				timeMax = getTime().toString();
				query = query.replace(word, "").trim();
				break;
				
			} else {
				timeMin = getTime().toString();
				if( Pattern.compile(Pattern.quote("next"), Pattern.CASE_INSENSITIVE).matcher(word).find() ) {
					query = query.replace(word, "").trim();
					break;
				}
			}
		}
		if(timeMin.trim().length() > 0) {
			this.OPTIONS += "&timeMin=" + timeMin;
		}
		if(timeMax.trim().length() > 0) {
			this.OPTIONS += "&timeMax=" + timeMax;
		}
//		System.out.println(query);
		return query;
	}
	
	private DateTime getTime() {
		return new DateTime().withTimeAtStartOfDay();
	}
}

package com.org.chatup.model;

/**
 * Created by nadiia.
 */
public class Constants {
    public static final String CHARACTER_SPACE_REGX = "\\s+";
    public static final String CHARACTER_SINGLE_QUOTE = "'";
    public static final String CHARACTER_SLASH = "/";
    public static final String CHARACTER_SPACE = " ";
    public static final String CHARATCER_NEWLINE = "\n";
    public static final String CHARATCER_HYPHEN = "-";
    public static final String CHARACTER_AMPERSAND = "&";
    public static final String CHARACTER_EQUALS = "=";
    public static final String CHARACTER_QUESTION_MARK = "?";
}

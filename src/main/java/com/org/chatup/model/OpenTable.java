package com.org.chatup.model;

import com.google.common.collect.Maps;
import com.org.chatup.User;
import com.org.chatup.Util.TextUtils;
import com.org.chatup.opentable.*;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.xpath.XPathExpressionException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.text.ParseException;
import java.util.*;

import static com.org.chatup.opentable.OpenTableConstants.DATE_PATTERN;

public class OpenTable {

	private static final Logger LOG = LoggerFactory.getLogger(OpenTable.class);

	private String query;
	private ReservationClient reservationClient;
	private OpenTableApi openTableApi;

	private User user;
	private Double latitude;
	private Double longitude;

	public OpenTable(String query, Double latitude, Double longitude, User user) {
		this.query = query;
		this.latitude = latitude;
		this.longitude = longitude;
		this.user = user;
		this.openTableApi = new OpenTableApi();
	}

	public String queryOpentable() throws IOException, URISyntaxException, XPathExpressionException, JSONException {
		try {
			if (query.toLowerCase().startsWith(OpenTableConstants.EAT)) {
				LOG.debug("Find restaurants request");
				return returnRestaurants();
			} else {
				String result = OpenTableConstants.RESTAURANTS_NOT_FOUND_MSG;
				String[] splits = query.split(Constants.CHARACTER_SPACE_REGX);
				LOG.debug("Query : " + query);
				if (splits.length > 0) {
					if (TextUtils.isInteger(splits[0])) {
						result = handleReservation(splits);
					} else {
						// parser line : name zip
						Map<String, String> params = Maps.newHashMap();
						// name could consists of a few words
						boolean hasZip = splits.length > 1 && TextUtils.isInteger(splits[splits.length - 1]);
//						String name = getName(splits, hasZip);
						String name = splits[0];
						params.put(OpenTableConstants.PARAM_NAME, name);
						if (hasZip) {
							params.put(OpenTableConstants.PARAM_ZIP, splits[splits.length - 1]);
						}
						List<RestaurantInfo> restaurants = openTableApi.findRestaurants(params);
						result = generateRestaurantOutputList(restaurants);
					}
				}
				return result;
			}
		} catch (TimeNotAvailable e) {
			return String.format(OpenTableConstants.RESTAURANTS_NOT_AVAILABLE_MSG,
					OpenTableConstants.DATE_HOUR_MIN_FORMAT.format(e.getDate()));
		} finally {
			IOUtils.closeQuietly(reservationClient);
		}
	}

	private String getName(String[] splits, boolean hasZip) {
		String name = "";
		int utillIndexName = hasZip ? splits.length - 1 : splits.length;
		for (int i = 0; i < utillIndexName; i++) {
            if (i != 0) {
                name += Constants.CHARACTER_SPACE;
            }
            name += splits[i];
        }
		return name;
	}

	private String handleReservation(String[] splits) throws TimeNotAvailable, IOException, URISyntaxException {
		reservationClient = new ReservationClient();
		String result = OpenTableConstants.WRONG_INPUT;
		Query queryParams;
		try {
			queryParams = parseQuery(splits);
		} catch (ParseException e) {
			LOG.warn(e.getMessage(), e);
			return result;
		}

		if (hasDataToBookTable(queryParams)) {
			LOG.debug("Try to book table");
			boolean successful = reservationClient.makeReservation(queryParams, user.getEmail());
			LOG.debug("Reservation has {}", successful ? "finished successful" : "failed");
			if (successful) {
				return String.format(OpenTableConstants.RESERVATION_SUCCESSFUL_MSG, queryParams.people,
						OpenTableConstants.DATE_HOUR_MIN_FORMAT.format(queryParams.date));
			} else {
				return String.format(OpenTableConstants.RESERVATION_FAILED_MSG, queryParams.people,
						OpenTableConstants.DATE_HOUR_MIN_FORMAT.format(queryParams.date));
			}
		} else {
			LOG.debug("Check availability of table");
			result = returnAvailability(queryParams);
			LOG.debug("Availability for restaurant {} is {}", queryParams.rid, result);
		}
		return result;
	}

	private boolean hasDataToBookTable(Query queryParams) {
		return StringUtils.isNoneBlank(queryParams.name)
				&& StringUtils.isNoneBlank(queryParams.lastName)
				&& StringUtils.isNoneBlank(queryParams.phone);
	}

	private String returnAvailability(Query queryParams) {
		List<Date> availableTimes = null;
		try {
			availableTimes = reservationClient.findAvailableTimes(queryParams);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
		return generateAvailableTimeStr(queryParams, availableTimes);
	}

	private Query parseQuery(String[] splits) throws ParseException {
		if (splits.length < 1) {
			return null;
		}
		Query queryParams = new Query(splits[0]);
		if (splits.length > 1) {
			String date = null;
			String time = null;
			int lastPositionBeforeName = 0;
			for (int i = 1; i < splits.length; i++) {
				// try get date and time strings
				if (splits[i].contains(Constants.CHARACTER_SLASH) &&
						TextUtils.isValidDate(splits[i], DATE_PATTERN)) {
					date = splits[i];
					lastPositionBeforeName = i;
				} else if (hasAmPmEnding(splits[i])) {
						if (i > 1 && splits[i].trim().length() == OpenTableConstants.HOUR_AM.length()) {
							time = splits[i - 1] + StringUtils.SPACE + splits[i];
						} else {
							int beginAIndex = splits[i].length() - OpenTableConstants.HOUR_AM.length();
							String digitPart = splits[i].substring(0, beginAIndex);
							String aPart = splits[i].substring(beginAIndex);
							time = digitPart + StringUtils.SPACE + aPart;
						}
						lastPositionBeforeName = i;
				} else if (splits[i].endsWith(OpenTableConstants.PEOPLE) ||
						splits[i].endsWith(OpenTableConstants.PEOPLE_SHORT)) {
					if (i > 1 &&
							(
									splits[i].trim().length() == OpenTableConstants.PEOPLE.length() ||
											splits[i].trim().length() == OpenTableConstants.PEOPLE_SHORT.length()
							)
							&& TextUtils.isInteger(splits[i - 1])) {
						queryParams.people = Integer.parseInt(splits[i - 1]);
					} else {
						int endingLength = OpenTableConstants.PEOPLE_SHORT.length();
						if (splits[i].endsWith(OpenTableConstants.PEOPLE)) {
							endingLength = OpenTableConstants.PEOPLE.length();
						}
						String peopleString = splits[i].substring(0, splits[i].length() - endingLength);
						if (TextUtils.isInteger(peopleString)) {
							queryParams.people = Integer.parseInt(peopleString);
						}
					}
					lastPositionBeforeName = i;
				}
			}
			// try parse date and time
			if (date != null && time != null) {
				String dateString = date + " " + time;
				queryParams.date = DateUtils.parseDateStrictly(dateString, OpenTableConstants.DATE_HOUR_MIN_PATTERN,
						OpenTableConstants.DATE_HOUR_PATTERN);
			} else if (time != null) {
				queryParams.date = DateUtils.parseDateStrictly(time, OpenTableConstants.HOUR_MIN_PATTERN,
						OpenTableConstants.HOUR_PATTERN);
			} else if (date != null) {
				queryParams.date = DateUtils.parseDateStrictly(date, OpenTableConstants.DATE_PATTERN);
			}
			// try parse additional data for booking: name, lastname and phone
			int numberRequiredFields = 3;
			if (lastPositionBeforeName != 0
					&& lastPositionBeforeName + numberRequiredFields < splits.length) {
				queryParams.name = splits[lastPositionBeforeName + 1];
				queryParams.lastName = splits[lastPositionBeforeName + 2];
				queryParams.phone = splits[lastPositionBeforeName + 3];
			}
		}
		return queryParams;
	}

	private boolean hasAmPmEnding(String time) {
		return time.endsWith(OpenTableConstants.HOUR_AM) ||
				time.endsWith(OpenTableConstants.HOUR_PM);
	}

	private String generateAvailableTimeStr(Query queryParams, List<Date> availableTimes) {
		if (CollectionUtils.isNotEmpty(availableTimes)) {
			StringBuilder builder = new StringBuilder();
			String timeAvailableMsg = String.format(OpenTableConstants.RESTAURANTS_AVAILABLE_MSG, queryParams.people,
					OpenTableConstants.DATE_FORMAT.format(queryParams.date));
			builder.append(timeAvailableMsg);
			builder.append(Constants.CHARATCER_NEWLINE);
			for (int i = 0; i < availableTimes.size(); i++) {
				if (i > 0) {
					builder.append(Constants.CHARATCER_NEWLINE);
				}
				builder.append(OpenTableConstants.HOUR_MIN_FORMAT.format(availableTimes.get(i)));
			}
			builder.append(Constants.CHARATCER_NEWLINE);
			builder.append(OpenTableConstants.PEOPLE_NUMBER_INPUT);
			builder.append(Constants.CHARATCER_NEWLINE);
			builder.append(OpenTableConstants.BOOK_INPUT);
			return builder.toString();
		}
		return String.format(OpenTableConstants.RESTAURANTS_NOT_AVAILABLE_MSG,
				OpenTableConstants.DATE_HOUR_MIN_FORMAT.format(queryParams.date));
	}

	private String returnRestaurants() {
		String result = OpenTableConstants.RESTAURANTS_NOT_FOUND_MSG;
		List<RestaurantInfo> restaurants = new ArrayList<RestaurantInfo>();
		Map<String, String> params = new HashMap<String, String>();
		// query: eat <cuisine> <place>

		String[] splits = query.toLowerCase().trim()
				.split(Constants.CHARACTER_SPACE_REGX);
		int queryLength = splits.length;

		try {
			params.put(OpenTableConstants.PARAM_ZIP, LocationService.getDeviceLocation(latitude, longitude));

			switch (queryLength) {
				case 1:
					restaurants = openTableApi.findAllRestaurants(params);
					result = generateRestaurantOutputList(restaurants);
					break;
				case 2:
					restaurants = openTableApi.findRestaurantsByCuisine(params, splits[1]);
					result = generateRestaurantOutputList(restaurants);
					break;
				default:
					StringBuffer location = new StringBuffer();
					for (int i = 2; i < queryLength - 1; i++) {
						location.append(splits[i]);
						location.append("%20");
					}
					location.append(splits[queryLength - 1]);
					LOG.debug("location : " + location.toString().trim());
					params.remove(OpenTableConstants.PARAM_ZIP);
					params.put(OpenTableConstants.PARAM_CITY, location.toString()
							.trim());
					restaurants = openTableApi.findRestaurantsByCuisinePlace(params, splits[1]);
					result = generateRestaurantOutputList(restaurants);
					break;
			}
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
		return result;
	}

	private String generateRestaurantOutputList(List<RestaurantInfo> restaurants) {
		StringBuffer resultBuff = new StringBuffer();
		int count = 0;
		if (!restaurants.isEmpty()) {
			if (restaurants.size() > OpenTableConstants.MAX_RESTAURANT_COUNT) {
				count = OpenTableConstants.MAX_RESTAURANT_COUNT;
			} else {
				count = restaurants.size();
			}

			for (int i = 1; i <= count; i++) {
				resultBuff.append(i);
				resultBuff.append(Constants.CHARACTER_SPACE);
				resultBuff.append(restaurants.get(i - 1).getInfo());
				resultBuff.append(Constants.CHARATCER_NEWLINE);
			}
		}

		if (resultBuff.toString().isEmpty()) {
			resultBuff.append(OpenTableConstants.RESTAURANTS_NOT_FOUND_MSG);
		}
		LOG.debug("Filtered restaurants:\n" + resultBuff.toString());
		return resultBuff.toString();
	}

}

package com.org.chatup.model;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Set;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MovieHub {

	private static String apiURL = "";
	private static final String apiKey = "z3p8mb6jegt8vd5mqnt3rmfe";
	private static String latitude = "";
	private static String longitude = "";

	private static String mvTitle = "";
	private static String rating = "";
	private static String duration = "";
	private static String cast = "";
	private static String showTimes = "";

	private String movieDetails = "";
	
	private static JSONArray allMoviesFromZip = null;
	private static ArrayList<String> movieList = new ArrayList<>();

	private static JSONObject movieInfo = null;
	
	private static final String INVALID_MOVIE_MSG = "Please search with 'Movies near me' or 'Movies around me' \nelse key in the exact movie name";

	/**
	 * Method to determine which API to call based on the keyword received from the user.
	 * @param msg the message from the user.
	 * @param lat the latitude of the user's current location.
	 * @param lng the longitude of the user's current location.
	 */
	public String determineAPI (String msg, String lat, String lng) {

		latitude = lat;
		longitude = lng;
		if(msg.equalsIgnoreCase("near me") || msg.equalsIgnoreCase("around me") || msg.equalsIgnoreCase("movies around me") || msg.equalsIgnoreCase("movies near me")) {
			apiURL = "http://data.tmsapi.com/v1.1/movies/showings?startDate=" +getCurrentDateTime().get("Current Date")+"&lat="+lat+"&lng="+lng+"&api_key="+apiKey;
			return readUrlForAllMovies(apiURL);
		} else {
			msg = msg.replace(" ", "+");
			apiURL = "http://data.tmsapi.com/v1.1/programs/search?q="+msg+"&queryFields=title&entityType=movie&descriptionLang=en&api_key="+apiKey;
			String checkOneMvResp = readUrlForOneMovie(apiURL);
			if (!checkOneMvResp.isEmpty()) {
			return checkOneMvResp;
			} else {
				return INVALID_MOVIE_MSG;
			}
		}
	}

	/**
	 * Method to fetch the movies in the area of the user from the TMS API.
	 * @param url the url for fetching all movies by area from TMS API.
	 */
	public String readUrlForAllMovies(String url) {

		String result = invokeApi(url);
		String sendAllMovies = "";

		ArrayList<String> movieList = parseJsonAllMovies(result);
		for(int i=0; i<movieList.size(); i=i+2) {

			sendAllMovies = sendAllMovies + movieList.get(i) + "\n" +
					//"Duration: " + movieList.get(i+1) + ", " + "Rating: " + movieList.get(i+2) + "\n" +
					//"Cast: " + movieList.get(i+3) + "\n" +
					movieList.get(i+1) + "\n" + "**********************" + "\n";
		}
		return sendAllMovies;
	}

	/**
	 * Sets all the movie info like rating duration etc in a List
	 * @param result the string result from the API
	 * @return the List with movie information for all movies
	 */
	public ArrayList<String> parseJsonAllMovies(String result) {

		String theatreName = "";
		String timeSlot = "";
		String mvTheatreId = "";
		int countUniqueMovies = 0;
		int countShowTimes = 0;
		HashSet<String> rootIdSet = new HashSet<String>();
		String curTime = getCurrentDateTime().get("Current Time");
		int curHr = Integer.valueOf(curTime.substring(0,2));
		//int curMin = Integer.valueOf(curTime.substring(3,5));

		if (result != null && !result.isEmpty() && !result.equals("[]")) {
			try{
				// Population the JSON array from the json string.
				allMoviesFromZip = new JSONArray(result);

				//Looping through the all movies array
				for(int mv=0; mv<allMoviesFromZip.length();mv++) {
					HashSet<String> theatreIdSet = new HashSet<String>();
					JSONObject jso = allMoviesFromZip.getJSONObject(mv);
					String mvRootId =  jso.getString("rootId");
					//System.out.println("Movie Root Id: " +mvRootId);

					if (rootIdSet.add(mvRootId) && countUniqueMovies < 5){
						countUniqueMovies++;
						String cast = "";
						String slots = "";
						if (jso.getString("title")!=null && jso.getString("title")!="" && !jso.getString("title").isEmpty()) {
							mvTitle = jso.getString("title");
						}
						if (jso.has("runTime") && !jso.getString("runTime").isEmpty()) {
							duration = (jso.getString("runTime"));
						} else {
							duration = "NA";
						}
						if (jso.has("ratings") && jso.getJSONArray("ratings").length()!=0) {
							rating = jso.getJSONArray("ratings").getJSONObject(0).getString("code");
						}else{
							rating = "NA";
						}
						if (jso.has("topCast") && jso.getJSONArray("topCast").length()!=0) {
							cast += jso.getJSONArray("topCast").getString(0);
							for (int c = 1; c < jso.getJSONArray("topCast").length(); c++) {
								cast += ", ";
								cast += jso.getJSONArray("topCast").getString(c);
							}
							setCast(cast);
						}else{
							setCast("NA");
						}
						if (jso.has("showtimes") && jso.getJSONArray("showtimes").length()!=0) {
							//int countShowTimes = 0;
							for(int i=0; i<jso.getJSONArray("showtimes").length(); i++) {
								//HashSet<String> theatreIdSet = new HashSet<String>();
								mvTheatreId = jso.getJSONArray("showtimes").getJSONObject(i).getJSONObject("theatre").getString("id");
								theatreIdSet.add(mvTheatreId);

								if(theatreIdSet.size() <=3) {
									theatreName = jso.getJSONArray("showtimes").getJSONObject(i).getJSONObject("theatre").getString("name");
									timeSlot = jso.getJSONArray("showtimes").getJSONObject(i).getString("dateTime").substring(11,16);
									int cmpSlotHr = Integer.valueOf(timeSlot.substring(0,2));
									//int cmpSlotMin = Integer.valueOf(timeSlot.substring(3,5));
									if(slots.contains(theatreName)) {
										if(cmpSlotHr >= curHr){
											if(countShowTimes!=0){
												slots = slots + ", " + timeSlot;
											}
											else{
												slots = slots + timeSlot;
												countShowTimes++;
											}

										}
									} else if (cmpSlotHr >= curHr) {
										slots = slots + "\n" + theatreName + ": " ;
										countShowTimes = 0;
										//if(cmpSlotHr >= curHr){
										slots = slots + timeSlot;
										countShowTimes++;
										//}
									} else {
										slots = slots + " ";
									}
								}
							}
							setShowTimes(slots);
						}else{
							setShowTimes("NA");
						}

						if (mvTitle!="" && !mvTitle.isEmpty())
							movieList.add(mvTitle);
//						if (duration!="" && !duration.isEmpty())
//							movieList.add(duration);
//						if (rating!="" && !rating.isEmpty())
//							movieList.add(rating);
//						if (!getCast().isEmpty() && getCast()!=null) {
//							movieList.add(getCast());
//						}
						if (!getShowTimes().isEmpty() && getShowTimes()!=null) {
							movieList.add(getShowTimes());
						}
					}
				}
			} catch(JSONException jse) {
				jse.printStackTrace();
			}
		} return movieList;
	}

	/**
	 * Method to parse JsonResponse from the API and extract the tms id.
	 * @param result
	 * @return ArrayList with single movie information
	 */
	public String readUrlForOneMovie(String url) {
		String result = invokeApi(url);
		
		String movieShowTimes = "";
		if (result != null && !result.isEmpty() && !result.equals("") && !result.equals("[]")) {
			try{
				movieInfo = new JSONObject(result);
				JSONArray hits = movieInfo.getJSONArray("hits");

				String tmsId ="";
				for (int i=0; i<hits.length(); i++) {
					JSONObject programs = hits.getJSONObject(i);
					if (programs.getJSONObject("program").getString("tmsId").contains("MV")) {
						tmsId = programs.getJSONObject("program").getString("tmsId"); 			
						movieShowTimes = getMovieShowtimes(tmsId);
					}
				}
			} catch(JSONException jse) {
				jse.printStackTrace();
			}
			//System.out.println(movieShowTimes);
		} return movieShowTimes;
	}

	/**
	 * Fetch the time slots for the movie given the tmsId
	 * @param tmsId the Id of the movie to fetch the time slots for
	 * @return the timeSlot string
	 */
	private String getMovieShowtimes(String tmsId) {

		// Avoids <h1>Developer over Qps</h1> error. Need to think of work around.
		try {
			Thread.sleep(1000);
		} catch (InterruptedException ie) {
			ie.printStackTrace();
		}
		String apiURL = "http://data.tmsapi.com/v1.1/movies/"+tmsId+"/showings?startDate="+getCurrentDateTime().get("Current Date")+"&lat="+latitude+"&lng="+longitude+"&api_key="+apiKey;
		String result = invokeApi(apiURL);
		return formatJsonResponseForOneMovie(result);

	}

	/**
	 * Method to format JsonResponse.
	 * @param response the response from API.
	 */
	private String formatJsonResponseForOneMovie (String response) {

		LinkedList<LinkedHashMap<String, String>> showtimesList = new LinkedList<>();

		//String theatreId = "";
		String theatreName = "";
		String showtime = "";
		String movieName = "";

		if (!response.equals("") && !response.isEmpty() && !response.equals("[]")) {
			try{
				JSONArray movieShowTimesInfo = new JSONArray(response);

				JSONArray showTimes = movieShowTimesInfo.getJSONObject(0).getJSONArray("showtimes");
				movieName = movieShowTimesInfo.getJSONObject(0).getString("title");
				//System.out.println("Movie Name: "+movieName);
				//fullMovieData = movieName;
				LinkedHashMap<String, String> showTimeHashMap = new LinkedHashMap<>();
				showTimeHashMap.put("Movie Name", movieName);
				for(int i=0; i<showTimes.length(); i++) {
					//theatreId = showTimes.getJSONObject(i).getJSONObject("theatre").getString("id");
					theatreName = showTimes.getJSONObject(i).getJSONObject("theatre").getString("name");
					showtime = showTimes.getJSONObject(i).getString("dateTime").substring(11,16);

					if(!showTimeHashMap.containsKey(theatreName)){
						showTimeHashMap.put(theatreName, showtime);
					}else{
						String timeList = "";
						timeList = timeList + showTimeHashMap.get(theatreName)+ ", " + showtime;
						showTimeHashMap.put(theatreName, timeList);
					}

				}
				showtimesList.add(showTimeHashMap);

			} catch(JSONException jse) {
				jse.printStackTrace();
			}
		}		
		//String movieDetails = "";
		for(int i=0; i<showtimesList.size(); i++) {
			LinkedHashMap<String, String> tmpData = showtimesList.get(i);
			Set<String> key = tmpData.keySet();
			Iterator it = key.iterator();
			while (it.hasNext()) {
				String mvTheatreName = (String)it.next();
				String timing = tmpData.get(mvTheatreName);
				if (mvTheatreName.equals("Movie Name")) {
					movieDetails = movieDetails + timing + "\n";
				} 
				it.remove(); // avoids a ConcurrentModificationException

				if (!mvTheatreName.equals("Movie Name"))
					movieDetails = movieDetails + mvTheatreName + ": " +
							timing + "\n";
			}
			movieDetails = movieDetails + "**********************" + "\n";
			//System.out.println("****************************");
		}
		return movieDetails;

	}

	/**
	 * Getter for Cast of the movie
	 * @return cast the cast of the movie
	 */
	public static String getCast() {
		return cast;
	}

	/**
	 * Getter for showtimes of the movie
	 * @return showTimes the show times of the movie
	 */
	public static String getShowTimes() {
		return showTimes;
	}

	/**
	 * Setter for cast of the movie
	 * @param cast the cast of the movie
	 */
	public static void setCast(String cast) {
		MovieHub.cast = cast;
	}

	/**
	 * Setter for the show times of the movie
	 * @param showTimes the show times of the movie
	 */
	public static void setShowTimes(String showTimes) {
		MovieHub.showTimes = showTimes;
	}

	/**
	 * Method to fetch the system date.
	 * @return the current date as a string.
	 */
	private HashMap<String, String> getCurrentDateTime() {

		HashMap<String, String> currentDateTimeMap = new HashMap<String, String>();
		DateFormat todayDate = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
		Date today = Calendar.getInstance().getTime();
		String strDate = todayDate.format(today);
		String strTime = strDate.substring(11,16);
		String sDate = strDate.substring(0,10);
		currentDateTimeMap.put("Current Date", sDate);
		currentDateTimeMap.put("Current Time", strTime);
		return currentDateTimeMap;
	}
	
	/**
	 * Function to fire query to api and convert the response to string.
	 * @param url the String URL
	 * @return result the String response
	 */
	private String invokeApi(String url) {
		InputStream inputStream = null;
		String resultFromApi = "";

		try{
			HttpClient httpclient = HttpClientBuilder.create().build();
			HttpResponse httpResponse = httpclient.execute(new HttpGet(url));
			inputStream = httpResponse.getEntity().getContent();

			if(inputStream != null)
				resultFromApi = convertInputStreamToString(inputStream);
			else
				resultFromApi = "Did not work!";

		} catch (Exception e) {
			e.printStackTrace();
		} return resultFromApi;
	}

	/**
	 * Method to convert read the reply from the API.
	 * @param inputStream the InputStream object from API response.
	 * @return result the String object from InputStream object.
	 * @throws IOException
	 */
	private static String convertInputStreamToString(InputStream inputStream) throws IOException {
		BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
		String line = "";
		String result = "";
		while((line = bufferedReader.readLine()) != null)
			result += line;

		inputStream.close();
		return result;
	}

}
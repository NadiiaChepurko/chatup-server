package com.org.chatup.model;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.org.chatup.Util.Helper;

public class Uber {
	private final String UBER_SERVER_ACCESS_TOKEN = "3k_wFKCNb0DtEPeQFDu5l8WxPcQp3eAYogcVvNfx";
	private final String UBER_ENDPOINT = "https://api.uber.com/v1/";
	private double start_latitude, start_longitude, end_latitude, end_longitude;
	Map<String, String> headers;
	
	
	public Uber(double start_lat, double start_lng, double end_lat, double end_lng) {
		this.start_latitude = start_lat;
		this.start_longitude = start_lng;
		this.end_latitude = end_lat;
		this.end_longitude = end_lng;
		
		headers = new HashMap<String, String>();
		headers.put("Authorization", "Token " + this.UBER_SERVER_ACCESS_TOKEN);
	}
	
	
	public String getPrice() throws IOException, JSONException {
    	String result = null;
    	
    	String url = this.UBER_ENDPOINT + "estimates/price?"
    			+ "start_latitude=" + start_latitude
    			+ "&start_longitude=" + start_longitude
    			+ "&end_latitude=" + end_latitude
    			+ "&end_longitude=" + end_longitude
    			;
		JSONArray priceList;
		JSONObject json = Helper.readJsonFromUrl(url.replace(" ", "%20"), headers);
		
	    if(json.has("prices")) {
	    	priceList = json.getJSONArray("prices");
	    	
	    	if(priceList.length() > 0) {
	    		JSONObject priceItem = priceList.getJSONObject(0);
	    		
	    		result = "Uber: " + priceItem.getString("estimate");
	    		
	    		if(priceItem.getInt("surge_multiplier") > 1) {
	    			result += "Surge pricing";
	    		}
	    	}
	    }
    	
    	return result;
    }
}

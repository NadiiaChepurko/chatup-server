package com.org.chatup.model;

import com.beust.jcommander.internal.Lists;
import com.beust.jcommander.internal.Maps;
import com.google.common.collect.ImmutableList;
import com.org.chatup.Util.TextUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static com.org.chatup.model.Websites.*;

/**
 * Created by nadiia.
 */
public class Common {

    private static String MESSAGE_INPUT_OPTION = "Please input message again and add option numbery: word id query"
            + Constants.CHARATCER_NEWLINE;
    private static String MESSAGE_OPTIONS_NOT_FOUND = "Didn't find any options for inserted words, try another word!";
    private static Map<String, List<Pair<String,String>>> wordsMapWebsites = Maps.newHashMap();

    private String message;
    private String requestMsg;
    private List<Pair<String,String>> websites = Lists.newArrayList();

    static {
        List<Pair<String, String>> foodWebsites = Arrays.asList(
                Pair.of(YELP, "Find appropriate place to eat and drink!"),
                Pair.of(OPEN_TABLE, "Make restaurant reservation!")
        );

        List<Pair<String, String>> drinkWebsites = ImmutableList.<Pair<String, String>>builder()
                .addAll(foodWebsites)
                .add(Pair.of(WINESCAPES, "Find appropriate wines!"))
                .build();

        List<Pair<String, String>> transportWebsites = Arrays.asList(
                Pair.of(NJTRANSIT, "Find train station!"),
                Pair.of(NEXTBUS, "Find bus station!"),
                Pair.of(UBER, "Find taxi!")
        );

        List<Pair<String, String>> entertainmentWebsites = Arrays.asList(
                Pair.of(STUBHUB, "Buy ticket to social events, concerts and etc!"),
                Pair.of(MOVIES, "Find appropriate movie!")
        );

        wordsMapWebsites.put("eat", foodWebsites);
        wordsMapWebsites.put("starve", foodWebsites);

        wordsMapWebsites.put("drink", drinkWebsites);
        wordsMapWebsites.put("thirsty", drinkWebsites);

        wordsMapWebsites.put("drive", transportWebsites);
        wordsMapWebsites.put("go", transportWebsites);
        wordsMapWebsites.put("ride", transportWebsites);
        wordsMapWebsites.put("travel", transportWebsites);
        wordsMapWebsites.put("home", transportWebsites);

        wordsMapWebsites.put("entertainment", entertainmentWebsites);
        wordsMapWebsites.put("fun", entertainmentWebsites);
        wordsMapWebsites.put("movie", entertainmentWebsites);
        wordsMapWebsites.put("film", entertainmentWebsites);
    }

    public Common(String message) {
        this.message = message;
    }

    public void analyseMessage() {
        if (!StringUtils.isBlank(message)) {
            String[] splits = message.split(Constants.CHARACTER_SPACE_REGX);
            if (splits.length == 1) {
                websites = wordsMapWebsites.get(splits[0]);
            } else if (splits.length >= 2 && TextUtils.isInteger(splits[1])) {
                String word = splits[0];
                // user input index starting from 1 but not from 0 so adjust it
                int index = Integer.parseInt(splits[1]) - 1;
                List<Pair<String, String>> websiteDescriptions = wordsMapWebsites.get(word);
                if (websiteDescriptions != null) {
                    for (int i = 0; i < websiteDescriptions.size(); i++) {
                        if (i == index) {
                            websites = Arrays.asList(websiteDescriptions.get(i));
                        }
                    }
                }
                if (splits.length > 2) {
                    int startRequestIndx = message.indexOf(splits[2]);
                    requestMsg = message.substring(startRequestIndx);
                }
            }
        }
    }

    public List<Pair<String,String>> getWebsites() {
        return websites;
    }

    public String getRequestMsg() {
        return requestMsg;
    }

    public String buildResultString(List<Pair<String,String>> websiteDescriptions) {
        String result = MESSAGE_OPTIONS_NOT_FOUND;
        if (CollectionUtils.isNotEmpty(websiteDescriptions)) {
            StringBuilder resultBuilder = new StringBuilder(MESSAGE_INPUT_OPTION);
            for (int i = 0; i < websiteDescriptions.size(); i++) {
                resultBuilder
                        .append(i + 1)
                        .append(StringUtils.SPACE)
                        .append(websiteDescriptions.get(i).getKey())
                        .append(StringUtils.SPACE)
                        .append(Constants.CHARATCER_HYPHEN)
                        .append(StringUtils.SPACE)
                        .append(websiteDescriptions.get(i).getValue())
                        .append(Constants.CHARATCER_NEWLINE);
            }
            result = resultBuilder.toString();
        }
        return result;
    }

/*
    public static List<Pair<String, String>> toList(Pair<String, String>... pairs) {
        ImmutableList.Builder<Pair<String, String>> builder = ImmutableList.builder();
        for (Pair<String, String> pair : pairs) {
            builder.add(pair);
        }
        return builder.build();
    }
*/

}

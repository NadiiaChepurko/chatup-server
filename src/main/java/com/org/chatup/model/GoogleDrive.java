package com.org.chatup.model;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.org.chatup.Util.Helper;

public class GoogleDrive {
	private String ACCESS_TOKEN;
	private static final String SINGLE_QUOTE = "'";
	private final String queryType = "?q=fullText contains ";
	private String mimeType = "mimeType = 'application/vnd.google-apps.document'";
	private String query;
	private final String DRIVE_ENDPOINT = "https://www.googleapis.com/drive/v2/files";
	private Map<String, String> headers;
	
	public GoogleDrive(String query, String access_token) {
		this.ACCESS_TOKEN = access_token;
		this.query = query.replaceAll("[^\\s\\w]", "");
		
		headers = new HashMap<String, String>();
		headers.put("Authorization", "Bearer " + this.ACCESS_TOKEN);
	}
	
	public String getFiles() throws IOException, JSONException {
		String url = DRIVE_ENDPOINT + queryType + SINGLE_QUOTE + query + SINGLE_QUOTE + " and " + mimeType + "&access_token=" + ACCESS_TOKEN;
		String response = null;
		JSONArray files;
		
		JSONObject json = Helper.readJsonFromUrl(url.replace(" ", "%20"), null);
	    
	    if(json.has("items")) {
	    	files = json.getJSONArray("items");
	    	if(files.length() > 1) {
		    	response = readFiles(files);
		    }
	    	
	    } else if(json.has("error")) {
	    	JSONObject errorJson = json.getJSONObject("error");
	    	System.out.println(errorJson);
	    	int errorCode = errorJson.getInt("code");
	    	
	    	switch(errorCode) {
	    	
	    	case 401:
	    		response = "";
	    		break;
	    	
	    	case 403:
	    		response = "Authentication error. Try again later.";
	    		break;
	    	
	    	case 404:
	    		response = "File not found.";
	    		break;
	    	}
	    	
	    } else {
	    	response = "Something went wrong. Try again later.";
	    }
	    
	    if(response == null || response.trim().length() < 1) {
	    	response = "Sorry, nothing found";
	    }
	    
	    String output = response.trim();
	    if(output.length() > 1) {
	    	return "GOOGLE DRIVE\n" + response.trim();
	    }
	    
	    return "";
	}
	
	
	private String readFiles(JSONArray files) throws JSONException, MalformedURLException, IOException {
		String title;
		String fileContents;
		String searchResult;
		StringBuilder result = new StringBuilder();
		
		for(int i = 0; i < files.length(); i++) {
			JSONObject file = files.getJSONObject(i);
			
			if( ! file.has("title")) {
				continue;
			}
			title = file.getString("title");
			
			if(file.has("exportLinks")) {
				if(file.getJSONObject("exportLinks").has("text/plain")) {
					fileContents = Helper.readStringFromUrlAuth(file.getJSONObject("exportLinks").getString("text/plain"), headers);
					
					searchResult = searchQuery(fileContents);
					if(searchResult.trim().length() > 0) {
						result.append("File: " + title + "\n");
						result.append(searchQuery(fileContents) + "\n\n");
					}
				}
			}
		}
		
		return result.toString();
	}
	
	private String searchQuery(String fileContents) {
		StringBuilder sb = new StringBuilder();
		String[] words = query.split(" ");
		for(String word : words) {
			sb.append(word + "|");
		}
		if(words.length > 1) {
			sb.append(query + "|");
		}
		System.out.println(sb);
		StringBuilder result = new StringBuilder();
		Pattern pattern = Pattern.compile("[^.!;\\n]*(" + sb.substring(0, sb.length() - 1) + ")[^.!;\\n]*", Pattern.MULTILINE | Pattern.COMMENTS | Pattern.CASE_INSENSITIVE);
		Matcher matcher = pattern.matcher(fileContents);
		
		int count = 0;
		while(matcher.find() && count < 5) {
			result.append("--  " + matcher.group().trim() + "\n");
			count++;
		}
		
		return result.toString().trim();
	}
	
	
	
	
	
	
}

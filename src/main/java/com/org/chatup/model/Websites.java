package com.org.chatup.model;

/**
 * Created by nadiia.
 */
public interface Websites {

    String COMMON = "Common";
    String OPEN_TABLE = "Opentable";
    String OPEN311 = "Open311";
    String WINESCAPES = "Winescapes";
    String NJTRANSIT = "NJTransit";
    String YELP = "Yelp";
    String CLOUD_STORAGE = "Cloud Storage";
    String NEXTBUS = "NextBus";
    String GOOGLE_CALENDAR = "Google Calendar";
    String GOOGLE_TASKS = "Google Tasks";
    String STUBHUB = "StubHub";
    String MOVIES = "Movies";
    String UBER = "UBER";

}

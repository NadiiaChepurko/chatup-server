package com.org.chatup.model;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.org.chatup.Util.Helper;

public class StubHub {
	
	private String STUBHUB_ACCESS_TOKEN = "ptXGvQKUtrzY0HnVE1nBIwxjQ4Ma";
	private final String searchQuery = "?q=";
	private final String STUBHUB_ENDPOINT = "https://api.stubhubsandbox.com/search/catalog/events/v2";
	private static final int MAX_RESULTS = 5; 
	private String OPTIONS = "&status=active&sort=popularity desc&limit=" + MAX_RESULTS;
	Map<String, String> headers;
	
	public StubHub() {
		headers = new HashMap<String, String>();
		headers.put("Authorization", "Bearer " + this.STUBHUB_ACCESS_TOKEN);
	}
	
	
	public String getEvents(String query) throws IOException, JSONException {
    	String result = "";
    	
    	String url = this.STUBHUB_ENDPOINT + searchQuery + query + OPTIONS;
		JSONArray eventList;
		
		JSONObject json = Helper.readJsonFromUrl(url.replace(" ", "%20"), headers);

	    if(json.has("numFound")) {
	    	if(json.getInt("numFound") > 0 && json.has("events")) {
	    		eventList = json.getJSONArray("events");
		    	if(eventList.length() > 0) {
			    	result = getEventsDetails(eventList);
			    } else {
			    	result = "No events found";
			    }
	    	} else {
	    		result = "Sorry, try again later";
	    	}
	    }
    	
    	return result;
    }
    
    
    private String getEventsDetails(JSONArray eventList) throws JSONException {
    	String eventTitle, eventDateUTC, eventTicketPrice, eventTicketAvailability;
    	double eventLat, eventLong;
    	StringBuilder result = new StringBuilder();
		
    	for(int i = 0; i < eventList.length(); i++) {
    		JSONObject event = eventList.getJSONObject(i);
    		
    		eventTitle = event.getString("title");
    		eventDateUTC = event.getString("dateUTC");
    		
    		try {
    			SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ssZ");
				Date date = df.parse(eventDateUTC);
				eventDateUTC = date.toString();
			} catch (ParseException e) {
				e.printStackTrace();
			}
    		
    		JSONObject ticketInfo = event.getJSONObject("ticketInfo");
    		eventTicketPrice = ticketInfo.getDouble("minPrice") + " - " + ticketInfo.getDouble("maxPrice") + " " + ticketInfo.getString("currencyCode") + " each";
    		eventTicketAvailability = (int) ticketInfo.getDouble("totalTickets") + " tickets available";
    		
    		eventLat = event.getJSONObject("venue").getDouble("latitude");
    		eventLong = event.getJSONObject("venue").getDouble("longitude");
    		
    		result.append(eventTitle + "\n" + eventLat + ", " + eventLong + "\n" + eventDateUTC + "\n" + eventTicketAvailability + "\n" + eventTicketPrice + "\n\n");
    	}
		
    	return result.toString().trim();
    }

}

package com.org.chatup.model;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;


@WebServlet("/test")
public class test extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	PrintWriter output = response.getWriter();
    	Uber uber = new Uber(40.740790, -73.994310, 40.763287, -73.992336);
		try {
			output.write(uber.getPrice());
			
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
}

package com.org.chatup.model;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.MalformedURLException;
import java.nio.charset.Charset;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class OneDrive {
	private String ACCESS_TOKEN;
	private final String queryType = "?q=";
	private String query;
	private final String DRIVE_ENDPOINT = "https://api.onedrive.com/v1.0/drive/root/view.search";
	private final String DRIVE_DOWNLOAD_ENDPOINT = "https://api.onedrive.com/v1.0/drive/items/";
	
	public OneDrive(String query, String access_token) {
		this.ACCESS_TOKEN = access_token;
		this.query = query.replaceAll("[^\\s\\w]", "");
	}
	
	public String getFiles() throws IOException, JSONException {
		String url = DRIVE_ENDPOINT + queryType + query + "&access_token=" + ACCESS_TOKEN;
		String response = null;
		JSONArray files;
		
		JSONObject json = readJsonFromUrl(url.replace(" ", "%20"));
		
		if(json.has("value")) {
	    	files = json.getJSONArray("value");
	    	if(files.length() > 0) {
		    	response = readFiles(files);
		    }
	    	
	    } else if(json.has("error")) {
//	    	JSONObject errorJson = json.getJSONObject("error");
//	    	String errorMessage = errorJson.getString("message");
	    	response = "Something went wrong. Try again later.";
	    	
	    } else {
	    	response = "Something went wrong. Try again later.";
	    }
		
		String output = response.trim();
	    if(output.length() > 1) {
	    	return "ONEDRIVE\n" + response.trim();
	    }
	    
	    return "";
	}
	
	private static String readAll(Reader rd) throws IOException {
	    StringBuilder sb = new StringBuilder();
	    int cp;
	    while ((cp = rd.read()) != -1) {
	      sb.append((char) cp);
	    }
	    return sb.toString();
	  }
	
	public String readStringFromUrlAuth(String url, String authToken) throws ClientProtocolException, IOException {
		HttpClient httpClient = HttpClientBuilder.create().build();
		HttpGet httpRequest = new HttpGet(url);
		
		if(authToken != null) {
			httpRequest.setHeader("Authorization", "Bearer " + authToken);
		}
		
		HttpResponse httpResponse = httpClient.execute(httpRequest);
		
		BufferedReader rd = new BufferedReader(new InputStreamReader(httpResponse.getEntity().getContent(), Charset.forName("UTF-8")));
		String text = readAll(rd);
		
		return text;
	}

	public JSONObject readJsonFromUrl(String url) throws IOException, JSONException {
		String jsonText = readStringFromUrlAuth(url, null);
		JSONObject json = new JSONObject(jsonText);
		
		return json;
	}
	
	private String readFiles(JSONArray files) throws JSONException, MalformedURLException, IOException {
		String title;
		String fileContents;
		String searchResult;
		StringBuilder result = new StringBuilder();
		String id;

		for(int i = 0; i < files.length(); i++) {
			JSONObject file = files.getJSONObject(i);
			
			if( ! file.has("name") || ! file.has("id")) {
				continue;
			}
			title = file.getString("name");
			id = file.getString("id");
			
			if(file.has("webUrl")) {
				
				fileContents = readStringFromUrlAuth(DRIVE_DOWNLOAD_ENDPOINT + id + "/content", ACCESS_TOKEN);
				
				searchResult = searchQuery(fileContents);
				if(searchResult.trim().length() > 0) {
					result.append("File: " + title + "\n");
					result.append(searchResult + "\n\n");
				}
			}
		}
		
		return result.toString();
	}
	
	private String searchQuery(String fileContents) {
		StringBuilder sb = new StringBuilder();
		String[] words = query.split(" ");
		for(String word : words) {
			sb.append(word + "|");
		}
		if(words.length > 1) {
			sb.append(query + "|");
		}

		StringBuilder result = new StringBuilder();
		Pattern pattern = Pattern.compile("[^.!;\\n]*(" + sb.substring(0, sb.length() - 1) + ")[^.!;\\n]*", Pattern.MULTILINE | Pattern.COMMENTS | Pattern.CASE_INSENSITIVE);
		Matcher matcher = pattern.matcher(fileContents);
		
		int count = 0;
		while(matcher.find() && count < 5) {
			result.append("--  " + matcher.group().trim() + "\n");
			count++;
		}

		return result.toString().trim();
	}
}

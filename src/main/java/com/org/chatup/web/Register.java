package com.org.chatup.web;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import org.json.JSONException;
import org.json.JSONObject;

import com.org.chatup.Config;
import com.org.chatup.Util.Helper;


@WebServlet("/Register")
public class Register extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private static DataSource dataSource = null;
	
	@Override
	public void init() throws ServletException {
		super.init();
		
		dataSource = Config.getInstance(getServletContext()).getDataSource();
	}
	
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter output = response.getWriter();
		output.write("Hello there! Register doGet()");
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		JSONObject registerJson = Helper.getEncodedJson(request);
		JSONObject jObject = new JSONObject();
		String regId = "", result = "OK";
		
		if(registerJson != null && registerJson.has("gcm_regId")) {
			try {
				regId = registerJson.getString("gcm_regId");
				if( Helper.containsDeviceInfo(regId, dataSource)) {
					Helper.updateDeviceLastActiveTime(regId, dataSource);
					return ;
				}
				System.out.println("Registering new device: " + regId);
				
				String email = registerJson.getString("email");
				String name = registerJson.getString("name");
				Helper.insertDeviceInfo(name, email, regId, dataSource);
				
			} catch (JSONException e) {
				e.printStackTrace();
			} catch (SQLException e) {
				e.printStackTrace();
			} catch (InstantiationException | IllegalAccessException
					| ClassNotFoundException e) {
				e.printStackTrace();
			}
		}
		
		try {
			jObject.put("response", result);
			
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		PrintWriter writer = response.getWriter();
		writer.print(jObject);
		writer.close();
	}
}

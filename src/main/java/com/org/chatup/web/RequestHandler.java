package com.org.chatup.web;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.URISyntaxException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import javax.xml.xpath.XPathExpressionException;

import com.org.chatup.model.*;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.json.JSONException;
import org.json.JSONObject;

import com.gargoylesoftware.htmlunit.FailingHttpStatusCodeException;
import com.google.android.gcm.server.Constants;
import com.google.android.gcm.server.Message;
import com.google.android.gcm.server.Result;
import com.google.android.gcm.server.Sender;
import com.org.chatup.Config;
import com.org.chatup.User;
import com.org.chatup.Util.Helper;

import static com.org.chatup.model.Websites.*;


@WebServlet("/RequestHandler")
public class RequestHandler extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private static final String GOOGLE_SERVER_KEY = "AIzaSyCHnjTneDMtwTnYopGSf2NrRh1_9DcS9tI";
	static final String MESSAGE_KEY = "message";
	
	private static DataSource dataSource = null;

	@Override
	public void init() throws ServletException {
		super.init();
		
		dataSource = Config.getInstance(getServletContext()).getDataSource();
	}
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter output = response.getWriter();
		output.write("Hello there! RequestHandler doGet()");
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String result = "";
		String requestMsg = null;
		JSONObject requestJson, jObject = new JSONObject();
		
		try {
			System.out.println("Request received");
			requestJson = Helper.getEncodedJson(request);
			
			if(requestJson != null) {
				String regId = requestJson.getString("gcm_regId");
				String urlTitle = requestJson.getJSONObject("data").getString("urlTitle");
				String message = requestJson.getJSONObject("data").getString("message");
				double location_lat = requestJson.getJSONObject("location").getDouble("lat");
				double location_long = requestJson.getJSONObject("location").getDouble("lng");

				if (COMMON.equals(urlTitle)) {
					Common common = new Common(message);
					common.analyseMessage();
					if (common.getWebsites().size() == 1) {
						urlTitle = common.getWebsites().get(0).getKey();
						if (common.getRequestMsg() != null) {
							requestMsg = common.getRequestMsg();
							result = handleWebsiteRequest(requestJson, regId, urlTitle, requestMsg, location_lat, location_long);
						}
					} else {
						result = common.buildResultString(common.getWebsites());
					}
				} else {
					result = handleWebsiteRequest(requestJson, regId, urlTitle, message, location_lat, location_long);
				}

				JSONObject dataJson = new JSONObject()
						.accumulate("urlTitle", urlTitle)
						.accumulate("message", result);
				// add request msg to response when it is needed
				if (requestMsg != null) {
					dataJson.accumulate("request", message);
				}

				jObject.accumulate("data", dataJson);
				PrintWriter output = response.getWriter();
				output.write(jObject.toString());
				
				Sender sender = new Sender(GOOGLE_SERVER_KEY);
				Message msg = new Message.Builder().timeToLive(3600)
						.delayWhileIdle(true)
						.addData(MESSAGE_KEY, jObject.toString())
						.build();

				try {
					sendViaGcm(msg, sender, regId);
					
				} catch (InstantiationException | IllegalAccessException
						| ClassNotFoundException | SQLException e) {
					e.printStackTrace();
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private String handleWebsiteRequest(JSONObject requestJson, String regId, String urlTitle, String message,
										double location_lat, double location_long) throws JSONException, IOException,
			SQLException, URISyntaxException, XPathExpressionException {
		String result = StringUtils.EMPTY;

		switch(urlTitle) {
			case WINESCAPES:
				Winescapes winescapes = new Winescapes(message);
				result = winescapes.calculate();
				break;

			case NJTRANSIT:
				NJTransit njTransit = new NJTransit(message, dataSource);
	/*
				try {
					njTransit.scrapeStationList();
					njTransit.insertStationList(100);
				} catch (InstantiationException | IllegalAccessException
						| ClassNotFoundException | SQLException e) {
					e.printStackTrace();
				}
	*/
				try {
					result = njTransit.getTimings().trim();
					if(result.length() == 0) {
						result = "Could not find any trains. Try again in sometime";
					}
				} catch (ClassNotFoundException | FailingHttpStatusCodeException | InstantiationException | IllegalAccessException e) {
					e.printStackTrace();
				}
				break;

			case OPEN_TABLE:
				User user = getFirstUser(regId);
				OpenTable opentable = new OpenTable(message, location_lat, location_long, user);
				result = opentable.queryOpentable();
				break;

			case OPEN311:
				Open311 open311 = new Open311(message);
				result = open311.sendRequest();
				break;

			case YELP:
				Yelp yelp = new Yelp(message, Double.toString(location_lat), Double.toString(location_long));
				yelp.loadCategories();
				result = yelp.getPlaces();
				break;

			case CLOUD_STORAGE:
				if(requestJson.has("google_drive_access_token")) {
					String google_access_token = requestJson.getString("google_drive_access_token");
					GoogleDrive googleDrive = new GoogleDrive(message, google_access_token);
					result = googleDrive.getFiles();
				} else {
					result = "You must grant access to you Google Drive";
				}

				if(requestJson.has("one_drive_access_token")) {
					String onedrive_access_token = requestJson.getString("one_drive_access_token");
					OneDrive oneDrive = new OneDrive(message, onedrive_access_token);
					result = result + "\n\n" + oneDrive.getFiles();
				} else {
					result = result + "\n\n" + "You must grant access to you OneDrive";
				}
				break;

			case NEXTBUS:
				NextBus nxtbus = new NextBus(message, location_lat, location_long);
				result = nxtbus.getNextBus();
				break;

			case GOOGLE_CALENDAR:
				if(requestJson.has("google_calendar_access_token") ) {//&& requestJson.has("google_contacts_access_token")) {
					String google_calendar_access_token = requestJson.getString("google_calendar_access_token");
	//						String google_contacts_access_token = requestJson.getString("google_contacts_access_token");

	//						GoogleCalendar calendar = new GoogleCalendar(message, google_calendar_access_token, google_contacts_access_token);
					GoogleCalendar calendar = new GoogleCalendar(message, google_calendar_access_token, google_calendar_access_token);
					result = calendar.getEvents();
				} else {
					result = "Auth error";
				}
				break;

			case GOOGLE_TASKS:
				if(requestJson.has("google_tasks_access_token") && requestJson.has("google_tasks_access_token")) {
					String google_tasks_access_token = requestJson.getString("google_tasks_access_token");

					GoogleTasks tasks = new GoogleTasks(message, google_tasks_access_token);
	//						result = tasks.getEvents();
				} else {
					result = "Auth error";
				}
				break;

			case STUBHUB:
				StubHub stubHub = new StubHub();
				result = stubHub.getEvents(message);
				break;

			case MOVIES:
				MovieHub movieHub = new MovieHub();
				System.out.println(location_lat + "\t" + location_long);
				result = movieHub.determineAPI(message, String.valueOf(location_lat), String.valueOf(location_long));
				break;

			default:
				result = "This website is not supported right now.";
				break;
			}

		return result;
	}


	/**
	 * Sends the message using the Sender object to the registered device. 
	 */
	private void sendViaGcm(Message message, Sender sender, String regId) throws IOException, InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {
		
		try {
			// Check if device is registered
			if( ! Helper.containsDeviceInfo(regId, dataSource)) {
				return ;
			}
			
			Result result = sender.send(message, regId, 5);
			
			if(result.getMessageId() != null) {
				// Message was created successfully
				String canonicalRegId = result.getCanonicalRegistrationId();
				if(canonicalRegId != null) {
					// Same device has more than one regId. Update Database.
					User user = getFirstUser(regId);

					Helper.removeDeviceInfo(regId, dataSource);
					Helper.insertDeviceInfo(user.getName(), user.getEmail(), canonicalRegId, dataSource);
					
				} else {
					Helper.updateDeviceLastActiveTime(regId, dataSource);
				}
			} else {
				String error = result.getErrorCodeName();
				if(error.equals(Constants.ERROR_NOT_REGISTERED)) {
					// Client has been uninstalled - remove this device from Database.
					Helper.removeDeviceInfo(regId, dataSource);
				}
			}
			
		} catch(SQLException e) {
			e.printStackTrace();
			throw e;
			
		}
	}

	private User getFirstUser(String regId) throws SQLException {
		List<User> userList1 = new ArrayList<User>();
		userList1 = Helper.getDeviceInfo(regId, dataSource);
		List<User> userList = userList1;
		return userList.get(0);
	}

}

package com.org.chatup;

public class User {
	private String name;
	private String email;
	private boolean is_active;
	private String last_active;
	private String gcm_regId;
	
	public String getName() {
		return this.name;
	}
	
	public String getEmail() {
		return this.email;
	}
	
	public boolean getIsActive() {
		return this.is_active;
	}
	
	public String getLastActive() {
		return this.last_active;
	}
	
	public String getGcmRegId() {
		return this.gcm_regId;
	}
	
	
	public void setName(String name) {
		this.name = name;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public void setIsActive(boolean is_active) {
		this.is_active = is_active;
	}
	
	public void setLastActive(String last_active) {
		this.last_active = last_active;
	}
	
	public void setGcmRegId(String gcm_regId) {
		this.gcm_regId = gcm_regId;
	}
}

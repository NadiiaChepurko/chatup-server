package com.org.chatup.opentable;

import com.org.chatup.model.Constants;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClients;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by nadiia.
 */
public class LocationService {
    private static final Logger LOG = LoggerFactory.getLogger(LocationService.class);

    // returns zip for lat, long
    public static String getDeviceLocation(Double latitude, Double longitude) throws ClientProtocolException,
            IOException, JSONException {
        HttpGet httpGet = new HttpGet(
                "http://maps.google.com/maps/api/geocode/json?latlng="
                        + latitude + "," + longitude + "&sensor=false");
        HttpResponse response;
        HttpClient client = HttpClients.createDefault();
        StringBuilder stringBuilder = new StringBuilder();
        JSONObject jsonObject = null;
        response = client.execute(httpGet);
        HttpEntity entity = response.getEntity();
        InputStream stream = entity.getContent();
        int b;
        while ((b = stream.read()) != -1) {
            stringBuilder.append((char) b);
        }
        jsonObject = new JSONObject(stringBuilder.toString());
        String locality = fetchLocality(jsonObject);
        locality = convertToHttpFormat(locality);
        return locality;
    }

    private static String fetchLocality(JSONObject ret) throws JSONException {
        JSONObject location;
        String locality = new String();
        JSONArray resultArray = ret.getJSONArray("results");
        if (resultArray.length() > 0) {
            location = resultArray.getJSONObject(0);
            // Get the value of the attribute whose name is
            // "formatted_string"
            JSONArray components = location.getJSONArray("address_components");
            for (int i = 0; i < components.length(); i++) {
                JSONObject z = components.getJSONObject(i);
                JSONArray typesArray = z.getJSONArray("types");
                for (int j = 0; j < typesArray.length(); j++) {
                    if (typesArray.getString(j).equals(
                            OpenTableConstants.PARAM_POSTAL_CODE))
                        locality = z.getString("long_name");
                }
            }
        }
        LOG.debug("Locality " + locality);
        return locality;
    }

    private static String convertToHttpFormat(String locality) {
        StringBuffer location = new StringBuffer();
        String[] splits = locality.split(Constants.CHARACTER_SPACE_REGX);
        for (int i = 0; i < splits.length - 1; i++) {
            location.append(splits[i]);
            location.append("%20");
        }
        location.append(splits[splits.length - 1]);
        return location.toString();
    }

}

package com.org.chatup.opentable;

import java.util.Date;

/**
 * Created by nadiia.
 */
public class TimeNotAvailable extends Exception {

    private Date date;

    public TimeNotAvailable(String message, Date date) {
        super(message);
        this.date = date;
    }

    public Date getDate() {
        return date;
    }
}

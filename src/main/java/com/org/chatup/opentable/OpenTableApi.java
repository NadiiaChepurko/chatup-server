package com.org.chatup.opentable;

import com.beust.jcommander.internal.Maps;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.HttpClients;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.tidy.Tidy;

import javax.xml.xpath.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by nadiia.
 */
public class OpenTableApi {

    private static final Logger LOG = LoggerFactory.getLogger(OpenTableApi.class);

    public List<RestaurantInfo> findAllRestaurants(Map<String, String> params)
            throws XPathExpressionException, IOException, JSONException, URISyntaxException {
        LOG.debug("Finding all restaurants in the locality.");
        List<RestaurantInfo> restaurants = new ArrayList<RestaurantInfo>();

        for (RestaurantInfo r : findRestaurants(params, "")) {
            if (r.isGood() && r.isAvailable()) {
                restaurants.add(r);
            }
        }
        LOG.debug("Restaurants found in locality "
                + restaurants.size());
        return restaurants;
    }

    public List<RestaurantInfo> findRestaurantsByCuisinePlace(
            Map<String, String> params, String cuisine)
            throws XPathExpressionException, IOException, JSONException, URISyntaxException {
        LOG.debug("Finding restaurants by place and cuisine.."
                + cuisine);
        List<RestaurantInfo> restaurants = new ArrayList<RestaurantInfo>();

        for (RestaurantInfo r : findRestaurants(params, cuisine)) {
            if (r.isGood() && (r.getCuisine().toLowerCase().contains(cuisine))
                    && (r.isAvailable())) {
                restaurants.add(r);
            }
        }
        LOG.debug("Restaurants found by cuisine, place "
                + restaurants.size());
        return restaurants;
    }

    public List<RestaurantInfo> findRestaurantsByCuisine(Map<String, String> params, String cuisine)
            throws XPathExpressionException, IOException, JSONException, URISyntaxException {
        LOG.debug("Finding restaurants by cuisine.." + cuisine);
        List<RestaurantInfo> restaurants = new ArrayList<RestaurantInfo>();
        List<RestaurantInfo> res = findRestaurants(params, cuisine);
        for (RestaurantInfo r : res) {
            if (r.isGood() && (r.getCuisine().toLowerCase().contains(cuisine))
                    && (r.isAvailable())) {
                restaurants.add(r);
            }
        }
        LOG.debug("Restaurants found by cuisine " + restaurants.size());
        return restaurants;
    }

    private List<RestaurantInfo> findRestaurants(Map<String, String> params, String cuisine)
            throws XPathExpressionException, IOException, JSONException, URISyntaxException {
        LOG.debug("Finding all restaurants..");
        List<RestaurantInfo> restaurants = callFindRestaurants(params);
        restaurants = scrapeInformation(restaurants, cuisine);
        return restaurants;
    }

    public List<RestaurantInfo> findRestaurants(Map<String, String> params)
            throws XPathExpressionException, IOException, JSONException, URISyntaxException {
        LOG.debug("Finding restaurants..");
        List<RestaurantInfo> restaurants = callFindRestaurants(params);
        return restaurants;
    }

    private List<RestaurantInfo> scrapeInformation(
            List<RestaurantInfo> restaurants, String desiredCuisine)
            throws IOException, XPathExpressionException {

        Tidy tidy = new Tidy();
        tidy.setXHTML(true);
        tidy.setShowErrors(0);
        tidy.setQuiet(true);
        tidy.setShowWarnings(false);
        XPath xpath = XPathFactory.newInstance().newXPath();

        XPathExpression expr = null;
        Document doc = null;

        URL url = null;
        HttpURLConnection con = null;

        int count = 0;
        for (RestaurantInfo r : restaurants) {
            url = new URL(r.getReserveUrl());
            con = (HttpURLConnection) url.openConnection();
            con.connect();
            boolean isError = con.getResponseCode() >= 400;
            if (!isError) {
                r.setGood(true);
                doc = tidy.parseDOM(url.openStream(), System.out);
                expr = xpath
                        .compile("//*[contains(text(), 'Cuisines:')]/following-sibling::span");
                String cuisine = (String) expr.evaluate(doc,
                        XPathConstants.STRING);
                LOG.debug(cuisine);
                r.setCuisine(cuisine);

                if (desiredCuisine.isEmpty()
                        || cuisine.toLowerCase().contains(
                        desiredCuisine.toLowerCase())) {
                    r.setAvailable(true);
                    count++;
                    if(count > OpenTableConstants.MAX_RESTAURANT_COUNT-1){
                        break;
                    }
                } else {
                    r.setAvailable(false);
                }
            } else {
                r.setGood(false);
            }
        }
        LOG.debug("Restaurants scraped: " + restaurants.size());
        return restaurants;
    }

    private List<RestaurantInfo> callFindRestaurants(Map<String, String> params)
            throws ClientProtocolException, IOException, JSONException, URISyntaxException {
        HttpClient client = HttpClients.createDefault();
        URI uri = buildFindRestaurantsUri(params);
        HttpGet get = new HttpGet(uri);
        LOG.debug("Calling opentable API.." + get.getURI());
        HttpResponse response = client.execute(get);
        StringBuffer result = processOutput(response);

        List<RestaurantInfo> restaurants = generateRestaurantList(result);

        LOG.debug("Restaurants found: " + restaurants.size());
        return restaurants;
    }

    private URI buildFindRestaurantsUri(Map<String, String> params) throws URISyntaxException, MalformedURLException {
        URIBuilder uriBuilder = new URIBuilder(OpenTableConstants.OPENTABLE_URL);
        for (Map.Entry<String, String> nameAndValue : params.entrySet()) {
            uriBuilder.addParameter(nameAndValue.getKey(), nameAndValue.getValue());
        }
        return uriBuilder.build();
    }

    private List<RestaurantInfo> generateRestaurantList(StringBuffer result)
            throws JSONException {
        List<RestaurantInfo> restaurants = new ArrayList<RestaurantInfo>();
        JSONObject jsonObj = new JSONObject(result.toString());
        JSONArray jsonArray = jsonObj
                .getJSONArray(OpenTableConstants.PARAM_RESTAURANTS);
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject explrObject = jsonArray.getJSONObject(i);
            RestaurantInfo r = new RestaurantInfo();
            r.setId(explrObject.getLong(OpenTableConstants.PARAM_ID));
            r.setName(explrObject.getString(OpenTableConstants.PARAM_NAME));
            r.setPhone(explrObject.getString(OpenTableConstants.PARAM_PHONE));
            r.setAddress(explrObject
                    .getString(OpenTableConstants.PARAM_ADDRESS));
            r.setCity(explrObject.getString(OpenTableConstants.PARAM_CITY));
            r.setState(explrObject.getString(OpenTableConstants.PARAM_STATE));
            r.setCountry(explrObject
                    .getString(OpenTableConstants.PARAM_COUNTRY));
            r.setArea(explrObject.getString(OpenTableConstants.PARAM_AREA));
            r.setPostalCode(explrObject
                    .getString(OpenTableConstants.PARAM_POSTAL_CODE));
            r.setMobileReserveUrl(explrObject
                    .getString(OpenTableConstants.PARAM_MOBILE_RESERVE_URL));
            r.setReserveUrl(explrObject
                    .getString(OpenTableConstants.PARAM_RESERVE_URL));
            restaurants.add(r);
        }
        return restaurants;
    }

    private StringBuffer processOutput(HttpResponse response)
            throws IOException {
        BufferedReader rd = new BufferedReader(new InputStreamReader(response
                .getEntity().getContent()));
        StringBuffer result = new StringBuffer();
        String line = "";
        while ((line = rd.readLine()) != null) {
            result.append(line);
        }
        return result;
    }

}

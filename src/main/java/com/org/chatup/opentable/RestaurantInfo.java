package com.org.chatup.opentable;

public class RestaurantInfo {

	private long id;
	private String name;
	private String phone;
	private String address;
	private String city;
	private String state;
	private String country;
	private String area;
	private String postalCode;

	private String cuisine;
	private boolean isAvailable;
	private String price;

	private String mobileReserveUrl;
	private String reserveUrl;
	
	private boolean isGood;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public String getCuisine() {
		return cuisine;
	}

	public void setCuisine(String cuisine) {
		this.cuisine = cuisine;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getMobileReserveUrl() {
		return mobileReserveUrl;
	}

	public void setMobileReserveUrl(String mobileReserveUrl) {
		this.mobileReserveUrl = mobileReserveUrl;
	}

	public String getReserveUrl() {
		return reserveUrl;
	}

	public void setReserveUrl(String reserveUrl) {
		this.reserveUrl = reserveUrl;
	}

	public String getInfo() {
		return this.id + "|" + this.name + "|" + getCuisineInfo() + "|" + this.address + "|" + this.postalCode;
	}

	private String getCuisineInfo() {
		return this.cuisine != null ? this.cuisine: OpenTableConstants.NA_MSG;
	}

	public boolean isGood() {
		return isGood;
	}

	public void setGood(boolean isGood) {
		this.isGood = isGood;
	}

	public boolean isAvailable() {
		return isAvailable;
	}

	public void setAvailable(boolean isAvailable) {
		this.isAvailable = isAvailable;
	}

}


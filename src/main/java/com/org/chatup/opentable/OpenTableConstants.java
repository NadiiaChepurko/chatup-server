package com.org.chatup.opentable;

import java.text.SimpleDateFormat;

/**
 * Created by nadiia.
 */
public class OpenTableConstants {

    public static String HOUR_MIN_PATTERN = "hh:mm aa";
    public static String HOUR_PATTERN = "hh aa";
    public static String DATE_PATTERN = "MM/dd/yyyy";
    public static String DATE_HOUR_MIN_PATTERN = DATE_PATTERN + " " + HOUR_MIN_PATTERN;
    public static String DATE_HOUR_PATTERN = DATE_PATTERN + " " + HOUR_PATTERN;

    public static SimpleDateFormat HOUR_MIN_FORMAT = new SimpleDateFormat(HOUR_MIN_PATTERN);
    public static SimpleDateFormat DATE_FORMAT = new SimpleDateFormat(DATE_PATTERN);
    public static SimpleDateFormat DATE_HOUR_MIN_FORMAT = new SimpleDateFormat(DATE_HOUR_MIN_PATTERN);


    public static final int MAX_RESTAURANT_COUNT = 5;
    public static final int DEFAULT_POEPLE_NUMBER = 2;

    public static final String OPENTABLE_URL = "http://opentable.herokuapp.com/api/restaurants";

    public static final String PARAM_ZIP = "zip";
    public static final String PARAM_RESERVE_URL = "reserve_url";
    public static final String PARAM_MOBILE_RESERVE_URL = "mobile_reserve_url";
    public static final String PARAM_POSTAL_CODE = "postal_code";
    public static final String PARAM_AREA = "area";
    public static final String PARAM_COUNTRY = "country";
    public static final String PARAM_STATE = "state";
    public static final String PARAM_CITY = "city";
    public static final String PARAM_ADDRESS = "address";
    public static final String PARAM_PHONE = "phone";
    public static final String PARAM_NAME = "name";
    public static final String PARAM_RESTAURANTS = "restaurants";
    public static final String PARAM_ID = "id";
    public static final String PARAM_LOCALITY = "locality";

    public static final String EAT = "eat";
    public static final String PEOPLE = "people";
    public static final String PEOPLE_SHORT = "p";
    public static final String HOUR_PM = "pm";
    public static final String HOUR_AM = "am";

    public static final int TIMEOUT = 1;


    public static final String NA_MSG = "N/A";
    public static final String RESERVATION_SUCCESSFUL_MSG = "Sent reservation request for %s people on %s. " +
            "You should receive confirmation on email box";
    public static final String RESERVATION_FAILED_MSG = "Sorry, could not send reservation request for %s people on %s. Try again later: id [mm/dd/yyyy] [time]";
	public static final String RESTAURANTS_NOT_FOUND_MSG = "Sorry, could not find any restaurant. Try the syntax: eat cuisine location";
    public static final String RESTAURANTS_NOT_AVAILABLE_MSG = "Sorry, no table available on %s. Try: id [mm/dd/yyyy] [time]";
    public static final String RESTAURANTS_AVAILABLE_MSG = "Table available for %s people on %s for time:";
    public static final String WRONG_INPUT = "Sorry, not correct input. Try: id [mm/dd/yyyy] [time]";
    public static final String PEOPLE_NUMBER_INPUT = "To change # of people try: id [mm/dd/yyyy] [time] [# people]";
    public static final String BOOK_INPUT = "To book table use: id [mm/dd/yyyy] time [# people] name lastname phone";

}

package com.org.chatup.opentable;

import java.util.Date;

/**
 * Created by nadiia.
 */
public class Query {

    public String rid;
    public int people;
    public Date date;

    public String name;
    public String lastName;
    public String phone;

    public Query(String rid) {
        this.rid = rid;
        this.date = new Date();
        this.people = OpenTableConstants.DEFAULT_POEPLE_NUMBER;
    }

}

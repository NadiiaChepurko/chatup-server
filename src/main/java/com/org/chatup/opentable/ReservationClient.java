package com.org.chatup.opentable;

import com.beust.jcommander.internal.Lists;
import com.org.chatup.model.Constants;
import org.apache.commons.io.FileUtils;
import org.apache.http.client.utils.URIBuilder;
import org.openqa.selenium.*;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriverService;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by nadiia
 */
public class ReservationClient implements Closeable {

    private static final Logger LOG = LoggerFactory.getLogger(ReservationClient.class);

    private WebDriver driver;
    private boolean takesScreenshot;
    private static String BASE_RESERVATION_URL = "http://www.opentable.com/opentables.aspx?t=Single&m=8";
    private static String QUERY_DATE_PATTERN = "MM/dd/yyyy hh:mm:ss aa";
    private static SimpleDateFormat QUERY_DATE_FORMAT = new SimpleDateFormat(QUERY_DATE_PATTERN);

    public ReservationClient() {
        this(false);
    }

    public ReservationClient(boolean takesScreenshot) {
        this.takesScreenshot = takesScreenshot;
        this.driver = createPhantomJS(takesScreenshot);
        this.driver.manage().timeouts().pageLoadTimeout(90, TimeUnit.SECONDS);
        this.driver.manage().timeouts().implicitlyWait(90, TimeUnit.SECONDS);
        this.driver.manage().timeouts().setScriptTimeout(90, TimeUnit.SECONDS);
    }

    public URL buildReservationUrl(String rid, int people, Date date) throws URISyntaxException, MalformedURLException {
        return new URIBuilder(BASE_RESERVATION_URL)
                .addParameter("rid", rid)
                .addParameter("p", String.valueOf(people))
                .addParameter("d", QUERY_DATE_FORMAT.format(date))
                .build().toURL();
    }

    public List<Date> findAvailableTimes(Query query) throws MalformedURLException, URISyntaxException {
        List<Date> availableTimes = Lists.newArrayList();
        try {
            URL url = buildReservationUrl(query.rid, query.people, query.date);
            // visit site url
            driver.navigate().to(url);
            saveScreenshot(driver, "findAvailableTimes");
            // Find available time slots
            List<WebElement> availableTimeEls = driver.findElements(By.xpath("//ul[@class='ResultTimes']/li[@a]"));
            for (WebElement availableTimeEl : availableTimeEls) {
                // ['3/28/2016 10:00:00 PM',100,0,0]
                try {
                    String attributeValue = availableTimeEl.getAttribute("a");
                    int startIndex = attributeValue.indexOf(Constants.CHARACTER_SINGLE_QUOTE);
                    int endIndex = attributeValue.lastIndexOf(Constants.CHARACTER_SINGLE_QUOTE);
                    if (startIndex != -1 && endIndex != -1) {
                        String dateTime = attributeValue.substring(startIndex + 1, endIndex);
                        availableTimes.add(QUERY_DATE_FORMAT.parse(dateTime));
                    }
                } catch (ParseException e) {
                    LOG.error("Can not parse available date time", e);
                }
            }
        } catch (TimeoutException e) {
            LOG.error(e.getMessage(), e);
        }
        return availableTimes;
    }

    public boolean makeReservation(Query query, String email) throws IOException, URISyntaxException, TimeNotAvailable {
        try {
            URL url = buildReservationUrl(query.rid, query.people, query.date);
            // visit site url
            driver.navigate().to(url);
            saveScreenshot(driver, "makeReservation_1");
            // we need to find particular time element and click on it to go on reservation page
            WebElement chosenTime = findExactlyReservationTimeElement(query.date);
            chosenTime.click();
            saveScreenshot(driver, "makeReservation_2");
            // complete reservation on opened page
            completeReservation(query.name, query.lastName, email, query.phone);
            saveScreenshot(driver, "makeReservation_4");
            return true;
        } catch (TimeoutException e) {
            LOG.error(e.getMessage(), e);
        }
        return false;
    }

    private WebElement findExactlyReservationTimeElement(Date date) throws IOException, URISyntaxException, TimeNotAvailable {
        try {
            WebElement exactAvailableTimeEl = driver.findElement(By.xpath("//ul[@class='ResultTimes']/li[@class='ti exact']/span"));
            return exactAvailableTimeEl;
        } catch (NoSuchElementException e) {
            throw new TimeNotAvailable("Element option with this time is not selected!", date);
        }
    }

    private void completeReservation(String name, String lastName, String email, String phone) throws IOException {
        // Find the text input element by its name
        WebElement firstNameEl = driver.findElement(By.id("firstName"));
        WebElement lastNameEl = driver.findElement(By.id("lastName"));
        WebElement emailEl = driver.findElement(By.id("email"));
        WebElement phoneEl = driver.findElement(By.id("phoneNumber"));
        WebElement submitButtonEl = driver.findElement(By.id("completeReservation"));
        WebElement phoneCountryCodeEl = driver.findElement(
//                By.xpath("//select[@id='countryCode_SelectDropDown']/option[@value='UA']"));
                By.xpath("//select[@id='countryCode_SelectDropDown']/option[@value='US']"));
        phoneCountryCodeEl.click();

        firstNameEl.sendKeys(name);
        lastNameEl.sendKeys(lastName);
        emailEl.sendKeys(email);
        phoneEl.sendKeys(phone);
        saveScreenshot(driver, "makeReservation_3");

        // Can not just submit the form. WebDriver should find the form for us from the element when
        // click submit but it does not work so just click button
        submitButtonEl.click();
    }

    private WebDriver createPhantomJS(boolean takesScreenshot) {
        DesiredCapabilities cp = new DesiredCapabilities();
        cp.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY,
                "/usr/local/bin/phantomjs");
        cp.setCapability("takesScreenshot", takesScreenshot);
        return new PhantomJSDriver(cp);
    }

    public void saveScreenshot(WebDriver driver, String name) {
        try {
            if (takesScreenshot) {
                File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
                // Now you can do whatever you need to do with it, for example copy somewhere
                FileUtils.copyFile(scrFile, new File("screenshots/screenshot_" + name + ".png"));
            }
        } catch (IOException e) {
            LOG.error(e.getMessage(), e);
        }
    }

    @Override
    public void close() {
        driver.quit();
    }
}

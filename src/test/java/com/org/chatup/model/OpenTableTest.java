package com.org.chatup.model;

import com.org.chatup.opentable.OpenTableConstants;
import com.org.chatup.opentable.Query;
import com.org.chatup.opentable.ReservationClient;
import com.org.chatup.opentable.TimeNotAvailable;
import org.apache.commons.lang3.time.DateUtils;
import org.junit.Ignore;
import org.junit.Test;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.text.ParseException;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

public class OpenTableTest {

    @Test
    @Ignore
    public void testBuildReservationUrl() throws MalformedURLException, URISyntaxException {
        ReservationClient client = new ReservationClient();
        String rId = "212734";
        URL url = client.buildReservationUrl(rId, 2, new Date());
        assertNotNull(url);
    }

    @Test
    @Ignore
    public void testFindAvailableTimes() throws MalformedURLException, URISyntaxException, ParseException {
        ReservationClient client = new ReservationClient(true);
        String rId = "212734";
        String date = "03/27/2016 3:00 PM";
        Query query = new Query(rId);
        query.date = DateUtils.parseDateStrictly(date, OpenTableConstants.DATE_HOUR_MIN_PATTERN);
        List<Date> availableTimes = client.findAvailableTimes(query);
        assertNotNull(availableTimes);
        assertFalse(availableTimes.isEmpty());
    }

//    @Test
    @Ignore
    public void testSSLReservationPage() throws IOException, URISyntaxException, TimeNotAvailable {
        OpenTable openTable = new OpenTable("", null, null, null);
        String rId = "212734";

        ReservationClient reservationClient = new ReservationClient(true);

        String email = "some@gmail.com";
        Query query = new Query(rId);
        query.name = "Name";
        query.lastName = "Sparacio";
        query.phone = "";

//        reservationClient.makeReservation(query, email);
    }

}
